---

<p id="footer">By [Lynn](https://gitlab.com/lynnpepin/), 2020. Posts are [CC BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/4.0/) except if otherwise specified.</p>
