---
title: "About this blog"
author:
 - Lynn
abstract: About page
date: 2020 Sept 9
lang: en-US
---

This is a simple static-site generator written in Python and Bash, using Pandoc. This is the "about" page. It can be written in Markdown like any other post!
