---
title: "My cool post!"
author:
 - Lynn
date: 2020-09-03
lang: en-US
category: opinion
---

# This is a cool post.

I mean, it isn't really.

% Is this a comment?

## Any data embedded in it?
Nope! No titles, no keywords, not even a date. After all, this takes only 10 minutes! Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lore em and ips um and all that jazz.

| Tables | Are | Cool |
| - | - | - |
| 1 | Hmm | >:) |
| 2 | A table | Cool! |
| 3 | Oh boy | Rad |

### Posts?

`owo`? What's `this` this?

    # take user input
    username = input("What are you?")
    
    # print out their username!
    if len(username) > 0:
        print(f"Hello world, I am {username}")
    else:
        print("Rude of you not to tell me your username...")
    
```
# take user input
username = input("What are you?")

# print out their username!
if len(username) > 0:
    print(f"Hello world, I am {username}")
else:
    print("Rude of you not to tell me your username...")
```

